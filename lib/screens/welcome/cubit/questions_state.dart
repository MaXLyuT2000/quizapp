part of 'questions_cubit.dart';

@immutable
abstract class QuestionsState {}

class QuestionsInitial extends QuestionsState {}

class QuestionsLoaded extends QuestionsState {
  final List<Question> questions;

  QuestionsLoaded(this.questions);
}

class QuestionsLoading extends QuestionsState {}

class QuestionsError extends QuestionsState {}

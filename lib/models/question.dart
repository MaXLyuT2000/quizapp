import 'package:flutter/cupertino.dart';

class Question {
  final int id;
  final String question;
  final String description;
  final Map<String, dynamic> answers;
  final String multipleCorrectAnswers;
  final Map<String, dynamic> correctAnswers;
  final String explanation;

  String category;
  String difficulty;

  Question(
      {@required this.id,
      @required this.question,
      @required this.description,
      @required this.answers,
      @required this.multipleCorrectAnswers,
      @required this.correctAnswers,
      @required this.explanation,
      @required this.category,
      @required this.difficulty});

  factory Question.fromJson(Map<String, dynamic> json) {
    return Question(
      id: json['id'],
      question: json['question'],
      description: json['description'],
      answers: (json['answers'] as Map<String, dynamic>)
        ..removeWhere((key, value) => value == null),
      multipleCorrectAnswers: json['multiple_correct_answers'],
      correctAnswers: json['correct_answers'] as Map<String, dynamic>,
      explanation: json['explanation'],
      category: json['category'],
      difficulty: json['difficulty'],
    );
  }

  int get indexOfCorrectAnswer {
    var list = correctAnswers.values.toList();
    var index = list.indexWhere((element) => element == "true");
    return index;
  }
}

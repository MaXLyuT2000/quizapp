import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:quizapp/models/question.dart';
import 'package:quizapp/screens/quiz/provider/quiz_provider.dart';
import 'package:quizapp/screens/quiz/widgets/option.dart';
import 'package:quizapp/theme.dart';

class QuestionCard extends StatelessWidget {
  const QuestionCard({
    Key key,
    this.question,
  }) : super(key: key);

  final Question question;

  @override
  Widget build(BuildContext context) {
    final _quizProvider = Provider.of<QuizProvider>(context, listen: true);
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.all(kDefaultPadding),
        padding: const EdgeInsets.all(kDefaultPadding),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 5,
              offset: const Offset(0, 3),
            ),
          ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(25),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
              child: Text.rich(
                TextSpan(
                  text: "Вопрос ${_quizProvider.questionNumber}",
                  style: Theme.of(context)
                      .textTheme
                      .headline4
                      .copyWith(color: Colors.black),
                  children: [
                    TextSpan(
                      text: "/${_quizProvider.questions.length}",
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: Colors.black),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: kDefaultPadding),
            Text(
              question.question,
              style: Theme.of(context)
                  .textTheme
                  .headline6
                  ?.copyWith(color: kBlackColor),
            ),
            const SizedBox(height: kDefaultPadding),
            const SizedBox(height: kDefaultPadding / 2),
            ...List.generate(
              question.answers.length,
              (index) => Option(
                index: index,
                text: question.answers.values.elementAt(index),
                press: () => _quizProvider.checkAns(question, index),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quizapp/api/questions_repository.dart';
import 'package:quizapp/screens/welcome/cubit/questions_cubit.dart';
import 'package:quizapp/screens/welcome/welcome_screen.dart';
import 'package:quizapp/theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MultiRepositoryProvider(
      providers: [
        RepositoryProvider(create: (_) => QuestionsRepository()),
      ],
      child: BlocProvider<QuestionsCubit>(
        create: (context) =>
            QuestionsCubit(context.read<QuestionsRepository>()),
        child: const MyApp(),
      )));
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Quiz App',
      theme: AppStyles.appTheme,
      home: const WelcomeScreen(),
    );
  }
}

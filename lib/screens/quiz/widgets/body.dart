import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:quizapp/screens/quiz/provider/quiz_provider.dart';
import 'package:quizapp/screens/quiz/widgets/question_card.dart';
import 'package:quizapp/screens/score/score_screen.dart';
import 'package:quizapp/screens/welcome/cubit/questions_cubit.dart';

class QuizBody extends StatefulWidget {
  const QuizBody({Key key}) : super(key: key);

  @override
  State<QuizBody> createState() => _QuizBodyState();
}

class _QuizBodyState extends State<QuizBody> {
  @override
  Widget build(BuildContext context) {
    final _quizProvider = Provider.of<QuizProvider>(context, listen: true);
    final _questionsCubit = BlocProvider.of<QuestionsCubit>(context);
    //Навигация при окончании теста.
    if (_quizProvider.quizEnd) {
      _questionsCubit.scoreCorrectAns = _quizProvider.scoreCorrectAns;
      _questionsCubit.scoreUncorrectAns = _quizProvider.scoreUncorrectAns;

      WidgetsBinding.instance.addPostFrameCallback((_) {
        Navigator.pop(context);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => const ScoreScreen()));
      });
    }
    return PageView.builder(
      physics: const NeverScrollableScrollPhysics(),
      controller: _quizProvider.pageController,
      onPageChanged: _quizProvider.updateTheQnNum,
      itemCount: _quizProvider.questions.length,
      itemBuilder: (context, index) =>
          QuestionCard(question: _quizProvider.questions[index]),
    );
  }
}

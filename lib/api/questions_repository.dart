import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:quizapp/models/question.dart';
import 'package:quizapp/models/quiz.dart';

class QuestionsRepository {
  static const questUrl = 'https://quizapi.io/api/v1/questions';

  final apiKey = 'j24WhINsXuMG7PszLmbkLHqRiXRoFnjRZrHxkwDa';
  final int limit = 10;
  final Dio _dio = Dio();

  Future<List<Question>> getQuizQuestions(
      String difficulty, String category) async {
    try {
      Response response = await _dio.get(questUrl, queryParameters: {
        'apiKey': apiKey,
        'limit': limit,
        'category': category,
        'difficulty': difficulty,
      });
      if (response.statusCode == 200) {
        var questions = response.data as List<dynamic>;
        return questions.map((json) => Question.fromJson(json)).toList();
      }
      return null;
    } catch (e) {
      return null;
    }
  }

  Future<void> sendQuizScore(Quiz quiz) async {
    try {
      final docResults = FirebaseFirestore.instance.collection('results').doc();

      final jsResult = {
        'dateTime': quiz.dateTime,
        'category': quiz.category,
        'difficulty': quiz.difficulty,
        'scoreCorrectAns': quiz.scoreCorrectAns,
        'scoreUncorrectAns': quiz.scoreUncorrectAns
      };
      return await docResults.set(jsResult);
    } catch (e) {
      throw Exception();
    }
  }
}

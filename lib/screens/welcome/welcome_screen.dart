import 'package:flutter/material.dart';
import 'package:quizapp/screens/welcome/widgets/drop_category.dart';
import 'package:quizapp/screens/welcome/widgets/drop_difficulty.dart';
import 'package:quizapp/screens/welcome/widgets/play_button.dart';
import 'package:quizapp/theme.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Spacer(),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
              padding: const EdgeInsets.all(kDefaultPadding),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: const Offset(0, 3),
                  ),
                ],
                color: Colors.white,
                borderRadius: BorderRadius.circular(25),
              ),
              child: Column(
                children: const [
                  DropCategory(),
                  DropDifficulty(),
                ],
              ),
            ),
            const SizedBox(
              height: 50,
            ),
            const PlayButton(),
            const Spacer()
          ],
        ),
      )),
    );
  }
}

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:quizapp/api/questions_repository.dart';
import 'package:quizapp/models/quiz.dart';

part 'save_score_state.dart';

class SaveScoreCubit extends Cubit<SaveScoreState> {
  final QuestionsRepository questionsRepository;
  SaveScoreCubit(this.questionsRepository) : super(SaveScoreInitial());

  bool _isLoading = false;

  void saveScore(Quiz quiz) async {
    if (_isLoading) return;
    emit(SaveScoreLoading());
    _isLoading = true;
    await questionsRepository
        .sendQuizScore(quiz)
        .then((value) => _isLoading = false);
    emit(SaveScoreLoaded());
  }
}

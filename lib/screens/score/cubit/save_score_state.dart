part of 'save_score_cubit.dart';

@immutable
abstract class SaveScoreState {}

class SaveScoreInitial extends SaveScoreState {}

class SaveScoreLoading extends SaveScoreState {}

class SaveScoreLoaded extends SaveScoreState {}

class SaveScoreError extends SaveScoreState {}

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:quizapp/api/questions_repository.dart';
import 'package:quizapp/models/question.dart';
import 'package:quizapp/models/quiz.dart';
part 'questions_state.dart';

class QuestionsCubit extends Cubit<QuestionsState> {
  final QuestionsRepository questionsRepository;

  List<Question> questions;

  String difficulty = '';
  String category = '';

  int scoreCorrectAns = 0;
  int scoreUncorrectAns = 0;

  bool _isLoading = false;

  final quizCategories = [
    'Linux',
    'DevOps',
    'Networking',
    'Programming',
    'Cloud',
    'Docker',
    'Kubernetes'
  ];

  Quiz get quizInfo {
    return Quiz(
        questions: questions,
        category: category,
        difficulty: difficulty,
        scoreCorrectAns: scoreCorrectAns,
        scoreUncorrectAns: scoreUncorrectAns,
        dateTime: DateTime.now());
  }

  final quizDifficulty = ['Easy', 'Medium', 'Hard'];

  QuestionsCubit(this.questionsRepository) : super(QuestionsInitial());

  void loadQuestions() async {
    if (_isLoading) return;
    emit(QuestionsLoading());
    _isLoading = true;
    await questionsRepository
        .getQuizQuestions(difficulty, category)
        .then((questions) {
      _isLoading = false;
      if (questions != null) {
        this.questions = questions;
        emit(QuestionsLoaded(questions));
      } else {
        emit(QuestionsError());
      }
    });
  }
}

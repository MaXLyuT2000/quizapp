import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quizapp/screens/welcome/cubit/questions_cubit.dart';

class DropCategory extends StatefulWidget {
  const DropCategory({Key key}) : super(key: key);

  @override
  State<DropCategory> createState() => _DropCategoryState();
}

class _DropCategoryState extends State<DropCategory> {
  @override
  void initState() {
    final quiz = BlocProvider.of<QuestionsCubit>(context);
    quiz.category = quiz.quizCategories.first;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final quiz = BlocProvider.of<QuestionsCubit>(context);

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          'Тема',
          style: Theme.of(context).textTheme.headline5,
        ),
        const SizedBox(
          width: 20,
        ),
        DropdownButton<String>(
            borderRadius: BorderRadius.circular(25),
            underline: const SizedBox(
              height: 0,
            ),
            isExpanded: false,
            items: quiz.quizCategories.map((theme) {
              return DropdownMenuItem(
                child: Text(
                  theme,
                  style: Theme.of(context).textTheme.headline6,
                ),
                value: theme,
              );
            }).toList(),
            value: quiz.category,
            onChanged: (quizSelectedCategory) => setState(() {
                  quiz.category = quizSelectedCategory;
                })),
      ],
    );
  }
}

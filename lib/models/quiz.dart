import 'package:quizapp/models/question.dart';

class Quiz {
  final String difficulty;
  final String category;
  final List<Question> questions;
  int scoreCorrectAns = 0;
  int scoreUncorrectAns = 0;
  DateTime dateTime;

  Quiz(
      {this.difficulty,
      this.category,
      this.questions,
      this.scoreCorrectAns = 0,
      this.scoreUncorrectAns = 0,
      this.dateTime});
}

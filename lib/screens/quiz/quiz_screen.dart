import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:quizapp/models/question.dart';
import 'package:quizapp/screens/quiz/provider/quiz_provider.dart';
import 'package:quizapp/screens/quiz/widgets/body.dart';

class QuizScreen extends StatelessWidget {
  const QuizScreen(Key key, this.questions) : super(key: key);

  final List<Question> questions;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SafeArea(
          child: ListenableProvider<QuizProvider>(
              create: (context) => QuizProvider(questions),
              child: const QuizBody())),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quizapp/api/questions_repository.dart';

import 'package:quizapp/screens/score/cubit/save_score_cubit.dart';
import 'package:quizapp/screens/welcome/cubit/questions_cubit.dart';

class SaveButton extends StatelessWidget {
  const SaveButton({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final saveCubit = SaveScoreCubit(context.read<QuestionsRepository>());
    final quiz = BlocProvider.of<QuestionsCubit>(context);

    return BlocProvider.value(
      value: saveCubit,
      child: ElevatedButton(
        onPressed: (() {
          saveCubit.saveScore(quiz.quizInfo);
        }),
        child: BlocConsumer<SaveScoreCubit, SaveScoreState>(
          listener: (context, state) {
            if (state is SaveScoreError) {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text("Произолша ошибка. Поторите попытку позже."),
                  backgroundColor: Colors.red,
                ),
              );
            }
            if (state is SaveScoreLoaded) {}
          },
          builder: (context, state) {
            if (state is SaveScoreLoading) {
              return const CircularProgressIndicator();
            }

            return const Text('Сохранить');
          },
        ),
      ),
    );
  }
}

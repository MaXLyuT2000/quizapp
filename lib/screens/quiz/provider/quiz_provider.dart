import 'package:flutter/cupertino.dart';
import 'package:quizapp/models/question.dart';

class QuizProvider extends ChangeNotifier {
  QuizProvider(this._questions) {
    _pageController = PageController();
  }

  //Вопросы викторины
  final List<Question> _questions;
  List<Question> get questions => _questions;

  int _scoreCorrectAns = 0;
  int get scoreCorrectAns => _scoreCorrectAns;
  int get scoreUncorrectAns => questions.length - _scoreCorrectAns;

  int _questionNumber = 1;
  int get questionNumber => _questionNumber;

  int _correctAns;
  int _selectedAns;

  bool _quizEdn = false;
  bool get quizEnd => _quizEdn;

  PageController _pageController;
  PageController get pageController => _pageController;

  void checkAns(Question question, int selectedIndex) {
    _correctAns = question.indexOfCorrectAnswer;
    _selectedAns = selectedIndex;
    if (_correctAns == _selectedAns) _scoreCorrectAns++;
    nextQuestion();
  }

  void nextQuestion() {
    if (_questionNumber != _questions.length) {
      _pageController.nextPage(
          duration: const Duration(milliseconds: 250), curve: Curves.ease);
    } else {
      _quizEdn = true;
      notifyListeners();
    }
  }

  void updateTheQnNum(int index) {
    _questionNumber = index + 1;
    notifyListeners();
  }
}

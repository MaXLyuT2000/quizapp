import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quizapp/screens/score/widgets/save_button.dart';
import 'package:quizapp/screens/welcome/cubit/questions_cubit.dart';
import 'package:quizapp/theme.dart';

class ScoreScreen extends StatelessWidget {
  const ScoreScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _quiz = BlocProvider.of<QuestionsCubit>(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
        child: Column(
          children: [
            const Spacer(flex: 2),
            Text(
              "Результаты",
              style: Theme.of(context)
                  .textTheme
                  .headline3
                  .copyWith(color: Colors.black),
            ),
            const Spacer(),
            Text(
              "${_quiz.scoreCorrectAns}/${_quiz.questions.length}",
              style: Theme.of(context)
                  .textTheme
                  .headline4
                  .copyWith(color: Colors.black),
            ),
            const Spacer(),
            Text(
              "Сложность: ${_quiz.difficulty}",
              style: Theme.of(context).textTheme.headline5,
            ),
            Text(
              "Тематика: ${_quiz.category}",
              style: Theme.of(context).textTheme.headline5,
            ),
            const Spacer(flex: 3),
            const SaveButton(),
            const Spacer(),
          ],
        ),
      ),
    );
  }
}

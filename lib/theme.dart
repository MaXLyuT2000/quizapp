import 'package:flutter/material.dart';

class AppStyles {
  static final appTheme = ThemeData(
    colorScheme: const ColorScheme.light(),
    fontFamily: 'Comfortaa',
    // ignore: prefer_const_constructors
    appBarTheme: AppBarTheme(
        elevation: 0,
        iconTheme: const IconThemeData(color: Colors.black),
        backgroundColor: Colors.transparent),

    elevatedButtonTheme: ElevatedButtonThemeData(
        style: TextButton.styleFrom(
            // elevation: 6,
            minimumSize: const Size(double.infinity, 46),
            backgroundColor: const Color.fromARGB(255, 22, 109, 223),
            padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 18),
            // side: BorderSide(color: Color(0xffC09E63), width: 3),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
            textStyle: const TextStyle(
                color: Colors.black,
                fontSize: 20,
                wordSpacing: 2,
                letterSpacing: 2))),
  );
}

const kSecondaryColor = Color(0xFF8B94BC);
const kGreenColor = Color(0xFF6AC259);
const kRedColor = Color(0xFFE92E30);
const kGrayColor = Color(0xFFC1C1C1);
const kBlackColor = Color(0xFF101010);
const kPrimaryGradient = LinearGradient(
  colors: [Color(0xFF46A0AE), Color(0xFF00FFCB)],
  begin: Alignment.centerLeft,
  end: Alignment.centerRight,
);

const double kDefaultPadding = 20.0;

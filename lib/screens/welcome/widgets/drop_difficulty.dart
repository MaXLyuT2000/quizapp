import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quizapp/screens/welcome/cubit/questions_cubit.dart';

class DropDifficulty extends StatefulWidget {
  const DropDifficulty({Key key}) : super(key: key);

  @override
  State<DropDifficulty> createState() => _DropDifficultyState();
}

class _DropDifficultyState extends State<DropDifficulty> {
  @override
  void initState() {
    final quiz = BlocProvider.of<QuestionsCubit>(context);
    quiz.difficulty = quiz.quizDifficulty.first;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final quiz = BlocProvider.of<QuestionsCubit>(context);

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          'Сложность',
          style: Theme.of(context).textTheme.headline5,
        ),
        const SizedBox(
          width: 20,
        ),
        DropdownButton<String>(
            borderRadius: BorderRadius.circular(25),
            underline: const SizedBox(
              height: 0,
            ),
            items: quiz.quizDifficulty.map((theme) {
              return DropdownMenuItem(
                child:
                    Text(theme, style: Theme.of(context).textTheme.headline6),
                value: theme,
              );
            }).toList(),
            value: quiz.difficulty,
            onChanged: (quizSelectedDifficulty) => setState(() {
                  quiz.difficulty = quizSelectedDifficulty;
                })),
      ],
    );
  }
}

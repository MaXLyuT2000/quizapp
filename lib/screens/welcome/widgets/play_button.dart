import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quizapp/screens/quiz/quiz_screen.dart';
import 'package:quizapp/screens/welcome/cubit/questions_cubit.dart';

class PlayButton extends StatelessWidget {
  const PlayButton({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      // style: Theme.of(context).elevatedButtonTheme.style,
      onPressed: (() {
        BlocProvider.of<QuestionsCubit>(context).loadQuestions();
      }),
      child: BlocConsumer<QuestionsCubit, QuestionsState>(
        listener: (context, state) {
          if (state is QuestionsError) {
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                content: Text("Произолша ошибка. Поторите попытку позже."),
                backgroundColor: Colors.red,
              ),
            );
          }
          if (state is QuestionsLoaded) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        QuizScreen(key, state.questions)));
          }
        },
        builder: (context, state) {
          if (state is QuestionsLoading) {
            return const CircularProgressIndicator();
          }

          return const Text(
            'Играть',
          );
        },
      ),
    );
  }
}
